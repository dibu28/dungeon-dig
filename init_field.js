main.prototype.initField = function() {
// first we destroy if needed then create new
// destroy arrays if exist
//if (this.player) {this.player.sprite.kill(); this.player.sprite.destroy();}
if (this.exit) {this.exit.sprite.kill(); this.exit.sprite.destroy();}
if (this.enemy){
	for (var i = 0; i < 15; i++)
	{
		if (this.enemy[i]){
		this.enemy[i].sprite.kill();
		this.enemy[i].sprite.destroy();
		}
	}
}
if (this.boon){
	for (var i = 0; i < 15; i++)
	{
		if (this.boon[i]){
		this.boon[i].sprite.kill();
		this.boon[i].sprite.destroy();
		}
	}
}

/// Now create new
	// game field
	this.field = new Array(7);
	// enemy and boon arrays
	this.enemy = new Array(15);
	this.boon  = new Array(15);
	//this.player = null;
	this.exit = null;

// create game field with 0
	for (var i = 0; i < 7; i++) {
		this.field[i] = new Array(11);
		for (var j = 0; j < 11; j++) {
		this.field[i][j] = 0;
		}
	}

// create entrance (player)
	var x = getRnd(0,6); 
	var y = 0;
	this.field[x][y] =new BasicGame.Entity('player',1) // need to be removed after generatio, or moved on sprite move.
	// Only if player does not exist or game over
	if (!this.player)
		{
//		    var sprite = this.game.add.sprite(this.xOf+(x*this.tSize), this.yOf+(y*this.tSize), 'fb7');
//		    sprite.animations.add('walk', Phaser.Animation.generateFrameNames('f_7435179fe1532ab7_', 0, 7, '', 3), 5, true);
		    var sprite = this.game.add.sprite((this.xOf+(x*this.tSize))-13, (this.yOf+(y*this.tSize))-15, 'fb8');
		    sprite.animations.add('walk', Phaser.Animation.generateFrameNames('f_4435179fe4198ab8_', 0, 13, '', 3), 3, true);
		    sprite.animations.play('walk');

//			this.player =new BasicGame.Player(this.game, this.game.add.sprite(this.xOf+(x*this.tSize), this.yOf+(y*this.tSize), 'atlas', 'explode'));
			this.player =new BasicGame.Player(this.game, sprite);
			this.level = 1; // level of game
		}
		else
		{
			this.player.sprite.x = this.xOf+(x*this.tSize)-13;
			this.player.sprite.y = this.yOf+(y*this.tSize)-15;
		}
// create exit
	var x = getRnd(0,6); 
	var y = 10;
	this.field[x][y] =new BasicGame.Entity('exit',1)
	this.exit =new BasicGame.Exit(this.game, this.game.add.sprite(this.xOf+(x*this.tSize), this.yOf+(y*this.tSize), 'atlas', 'explode'));

// create mob's
	for (var i = 0; i < 15; i++) {

		var x = getRnd(0,6); 
		var y = getRnd(0,10);
		if (this.field[x][y] == 0) // dirty
		{
			this.field[x][y] =new BasicGame.Entity('e',i);

		    var sprite = this.game.add.sprite((this.xOf+(x*this.tSize))+3, (this.yOf+(y*this.tSize))-3, 'fe4');
		    sprite.animations.add('walk', Phaser.Animation.generateFrameNames('f_4a57579a61de4_', 0, 6, '', 3), 3, true);
//		    sprite.animations.play('walk');

			this.enemy[i] =new BasicGame.Enemy(this.game, sprite);

//			this.enemy[i] =new BasicGame.Enemy(this.game, this.game.add.sprite(this.xOf+(x*this.tSize), this.yOf+(y*this.tSize), 'atlas', this.enemySprite[getRnd(0,3)]));
			this.enemy[i].life = getRnd(1,5+this.level*2); 
			this.enemy[i].power = getRnd(1,Math.floor(this.level/2)); //+Math.floor(this.level/3),2+Math.floor(this.level/2)); 
			this.enemy[i].expi = this.enemy[i].life * 20 + this.enemy[i].power * 50;
		}
	}
// create boons
	for (var i = 0; i < 15; i++) {

		var x = getRnd(0,6); 
		var y = getRnd(0,10);
		if (this.field[x][y] == 0) // dirty
		{
			this.field[x][y] =new  BasicGame.Entity('b',i);
			// only one boon at a time.
			if (getRnd(1,2) == 1)
			{
				this.boon[i] =new BasicGame.Boon(this.game, this.game.add.sprite(this.xOf+(x*this.tSize), this.yOf+(y*this.tSize), 'heart' ));
				this.boon[i].addLife = getRnd(-1,3);
			}
			else
			{
				this.boon[i] =new BasicGame.Boon(this.game, this.game.add.sprite(this.xOf+(x*this.tSize), this.yOf+(y*this.tSize), 'chest1')); //this.boonSprite[getRnd(0,3)])
				this.boon[i].sprite.animations.add('walk', Phaser.Animation.generateFrameNames('slice', 0, 4, '', 2), 5, true);
				this.boon[i].addPower = getRnd(-1,1); // adding alittle bit more power
			}
		}
	}
	/// remove player mark ? No. fixed in collision.
}