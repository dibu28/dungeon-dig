main.prototype.collision = function(x,y) {

    if ((this.field != 'undefined') && (this.field != null) && (this.field[x] != 'undefined') && (this.field[x] != null) && (this.field[x][y] != 'undefined') && (this.field[x][y] != null))
    {
	if (this.field[x][y] !=0)
	{
		console.log(this.field[x][y]);
		if (this.field[x][y].type=='e')
		{
			console.log(this.enemy[this.field[x][y].num]);
			if (this.enemy[this.field[x][y].num].life > 0)
			{
				this.eText.setText("Lives: " + this.enemy[this.field[x][y].num].life + " Power: " + this.enemy[this.field[x][y].num].power);
				this.enemy[this.field[x][y].num].life = this.enemy[this.field[x][y].num].life - this.player.power;
				if (this.enemy[this.field[x][y].num].life<=0)
				{
					/// kill enemy
					this.enemy[this.field[x][y].num].sprite.kill();
					this.player.expi = this.player.expi + this.enemy[this.field[x][y].num].expi;
					this.field[x][y] = 0;
					this.updateScore();
				}
				else
				{
					this.dText.setText("Lives: " + this.enemy[this.field[x][y].num].life + " Power: " + this.enemy[this.field[x][y].num].power);
					/// player receive damage on attack
					this.player.life = this.player.life - this.enemy[this.field[x][y].num].power;
					/// play enemy animation
					this.enemy[this.field[x][y].num].sprite.animations.play('walk',10,false,false);
					this.updateScore();
					if (this.player.life <= 0)
					{
						//console.log("game over!");
						//alert("game over!");
						if (this.player) {this.player.sprite.kill(); this.player.sprite.destroy();}
						this.player = null;
					        this.gText = this.game.add.text(70, this.game.world.centerY-100, "Game Over!", { font: "32px Arial", fill: "#0033ff", align: "center" });
					}
				}
			}
			// if we collided and can not move to the cell then
			return true;
		}
		if (this.field[x][y].type=='b')
		{	
			// play anim if any
			//if (this.boon[this.field[x][y].num].addLife > 0)
			//{
			//	this.boon[this.field[x][y].num].sprite.animations.play('walk');
			//}
			// for now just take and remove boon
			this.player.life = this.player.life + this.boon[this.field[x][y].num].addLife;
			this.player.power = this.player.power + this.boon[this.field[x][y].num].addPower;
			this.boon[this.field[x][y].num].sprite.kill();
			this.field[x][y] = 0;
			if (this.player.power < 1) {this.player.power = 1;} // power can't be less then 1.
			this.updateScore();
			// we are colliding here for the first time to take boon and chests and to see the animation.
			return true;
		}
		if (this.field[x][y].type=='exit')
		{
			/// Start next level
			this.level++;
			this.updateScore();
			this.initField ();
			return true;
		}
		if (this.field[x][y].type=='player')
		{
			return false;
		}
	}
    }
	return false;
}