main = function(){
}

main.prototype.start = function(){  //197, 308
	this.game = new Phaser.Game(320, 480, Phaser.AUTO, 'gameContainer', { preload: this.preload.bind(this), create: this.create.bind(this), update: this.update.bind(this) });
	this._callback = null;
	this.clicked = false;
	this.pressed = false;
	this.xOf = 50;
	this.yOf = 30;
	this.tSize = 32; // tile size width and height
	this.level = 1; // level of game
}

main.prototype.preload = function() {

	this.game.load.image('grid', 'atoms/grid.png');
	this.game.load.atlasXML('atlas', 'atoms/sprites.png', 'atoms/sprites.xml');

//	this.game.load.spritesheet('hall', 'atoms/orc.PNG',32,32);
	this.game.load.spritesheet('hall', 'atoms/vx-xp-181-switch04.png',32,32);
//	this.game.load.atlasXML('fb7', 'atoms/fb7.png', 'atoms/fb7.xml');
	this.game.load.atlasXML('fb8', 'atoms/fb8.png', 'atoms/fb8.xml');
	this.game.load.atlasXML('fe4', 'atoms/fe4.png', 'atoms/fe4.xml');

	this.game.load.image('heart', 'atoms/Hearts_026.png');
	this.game.load.image('dagger', 'atoms/dagger02.png');
	this.game.load.atlasXML('chest1', 'atoms/chest1.png', 'atoms/chest1.xml');

}

main.prototype.create = function() {

    	this.cursors = this.game.input.keyboard.createCursorKeys();
//set scaling
       	this.game.stage.backgroundColor = 0x0000ff;

        this.game.stage.scaleMode = Phaser.StageScaleMode.SHOW_ALL;
//       this.game.stage.scaleMode = Phaser.StageScaleMode.EXACT_FIT;
//       this.game.stage.scaleMode = Phaser.StageScaleMode.NO_SCALE;

        this.game.stage.scale.minWidth = 320;
        this.game.stage.scale.minHeight = 480;
        this.game.stage.scale.setMaximum();
        this.game.stage.scale.setShowAll();
//        this.game.stage.scale.setExactFit();
//        this.game.stage.scale.maxWidth = 960; //1024;
//        this.game.stage.scale.maxHeight = 640; //768;
        this.game.stage.scale.pageAlignHorizontally = true;
        this.game.stage.scale.pageAlignVertically = true;


//        this.game.stage.scale.refresh();
//	this.game.stage.scale.setSize();
	this.game.stage.scale.setScreenSize(); //true

/*
        if (this.game.device.android && this.game.device.chrome == false)
        {
            this.game.stage.scaleMode = Phaser.StageScaleMode.EXACT_FIT;
        }
*/


//game.world.height = 620;
//    this.sprite = this.game.add.tileSprite(0, 0, 197, 308, 'grid');
        this.lText = this.game.add.text(0, 0, "Lives:5 Power:3 Lvl:1", { font: "28px Arial", fill: "#ff0044", align: "left" });
        this.eText = this.game.add.text(0, 380, "Lives:1 Power:1", { font: "28px Arial", fill: "#ff0044", align: "left" });
        this.dText = this.game.add.text(0, 400, "Lives:1 Power:1", { font: "28px Arial", fill: "#ff0044", align: "left" });
//	this.sprite = this.game.add.sprite(this.xOf, this.yOf, 'grid');
//grid
	for (var i=0;i<7;i++)
	{
		for (var j=0;j<11;j++)
		{
			this.game.add.sprite(this.xOf+i*32, this.yOf+j*32, 'hall',6 ); //getRnd(0,5)
		}
	}


// sprites
this.enemySprite = ['atom1b','atom2b','atom3b','atom4b'];
this.boonSprite  = ['atom1a','atom2a','atom3a','atom4a',];

this.initField();

///
}

main.prototype.update = function() {

//	this.sprite.x = this.game.input.x;
    //  only move when you click
//	this.moveflag = false;
if (!this.clicked)
{
    if ((this.game.input.mousePointer.isDown)||(this.game.input.pointer1.isDown))
    {
	this.clicked = true;
//	this.moveflag = true;
//	this.mx = this.game.input.x
	//path(0,0,Math.round(this.game.input.x/this.tSize),Math.round(this.game.input.y/this.tSize), this.field, this);
        //  400 is the speed it will move towards the mouse
        //this.game.physics.moveToPointer(this.player.sprite, 400);

	// limit input
	if ((this.game.input.x >= this.xOf+0)&&(this.game.input.y>=this.yOf+0)&&(this.game.input.x<=this.xOf+(7*this.tSize))&&(this.game.input.y<=this.yOf+(11*this.tSize)))
	{
		if (!this.collision(Math.floor((this.game.input.x-this.xOf)/this.tSize),Math.floor((this.game.input.y-this.yOf)/this.tSize))) //,this
		{
		// move player
		this.player.sprite.x = (this.xOf+(Math.floor((this.game.input.x-this.xOf)/this.tSize)*this.tSize))-13;
		this.player.sprite.y = (this.yOf+(Math.floor((this.game.input.y-this.yOf)/this.tSize)*this.tSize))-15;
	}
	}
        //  if it's overlapping the mouse, don't move any more
        //if (Phaser.Rectangle.contains(this.player.sprite.body, this.game.input.x, this.game.input.y))
        //{
            //Phaser.Rectangle.contains(this.player.sprite.body.velocity.setTo(0, 0));
        //}
    }
    else
    {
        //this.player.sprite.body.velocity.setTo(0, 0);
    }
}
else
{
	if ((this.game.input.mousePointer.isUp)&&(this.game.input.pointer1.isUp))
	{
		this.clicked = false;
	}
}


if (!this.pressed)
{
	/// cursor keys
	if (this.cursors.left.isDown)
	{
		this.pressed = true;

		x = (this.xOf+(Math.floor((this.player.sprite.x-this.xOf)/this.tSize)*this.tSize))-32;
		y = (this.yOf+(Math.floor((this.player.sprite.y-this.yOf)/this.tSize)*this.tSize));

		if (!this.collision(Math.floor((x-this.xOf)/this.tSize),Math.floor((y-this.yOf)/this.tSize))) //,this
		{
			// move player
			this.player.sprite.x = (this.xOf+(Math.floor((x-this.xOf)/this.tSize)*this.tSize));
			this.player.sprite.y = (this.yOf+(Math.floor((y-this.yOf)/this.tSize)*this.tSize));
		}

	}
}
else
{
	if (this.cursors.left.isUp)
	{
		this.pressed = false;
	}
}
/*    if (this.game.input.Pointer1.isDown)
    {
	this.moveflag = true;
	
    }
*/

}

main.prototype.updateScore = function() {
	this.lText.setText("l:" + this.player.life + "p:" + this.player.power + "L:" + this.level + "x:" + this.player.expi);
}

///// inline functions

function getRnd (min, max){
	return Math.floor(Math.random() * (max - min + 1)) + min;
}

if (!Function.prototype.bind) { // check if native implementation available
  Function.prototype.bind = function(){ 
    var fn = this, args = Array.prototype.slice.call(arguments),
        object = args.shift(); 
    return function(){ 
      return fn.apply(object, 
        args.concat(Array.prototype.slice.call(arguments))); 
    }; 
  };
}

///// old for path

function path(x1,y1,x2,y2,field, th){

	this.easystar = new EasyStar.js();
	this.easystar.setGrid(field);
	this.easystar.setAcceptableTiles([0]);
	this.easystar.enableDiagonals();
	//this.easystar.setIterationsPerCalculation(1000); 
	this.easystar.findPath(x1, y1, x2, y2, pfc.bind(th));
	this.easystar.calculate();
	}

main.prototype.setCallbackFunction = function (callback) {
	this._callback = callback;
	}

pfc = function( path ){
    if (path === null){
        alert("Path was not found.");
    } else {
//        alert("Path was found. The first Point is " + path[0].x + " " + path[0].y + " = " + path[1].x + " " + path[1].y);
	for (var i = 0 ; i < path.length ; i++)
	{
		//
		this.game.add.sprite(path[i].x*this.tSize, path[i].y*this.tSize, 'atlas', 'explode');
	}
    }
}
